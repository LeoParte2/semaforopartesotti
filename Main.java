public class Main {

    public static void main (String[] args) {
        Semaforo sem1 = new Semaforo();
        Semaforo sem2 = new Semaforo();

        sem1.colore = "rosso";
        sem1.altezza = 3;
        sem1.peso = 2;
        sem1.disegno = "omino";
        sem1.suono = true;
        sem1.CheckColore();

        System.out.println(sem1);

        sem2.colore = "verde";
        sem2.altezza = 3;
        sem2.peso = 2;
        sem2.disegno = "cerchio";
        sem2.suono = false;
        sem2.CheckColore();

        System.out.println(sem2);

    }
}

